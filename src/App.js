import {useEffect, useState} from 'react';
import './App.css';
import Deletet from "./deletet";

function App() {

    const [products, setProducts] = useState(JSON.parse(localStorage.getItem('key')) || []);
    const [name, setName] = useState('');
    const [price, setPrice] = useState('');
    const [img, setImg] = useState('');
    const [filterActive, setFilterActive] = useState(true);
    const [searchText, setSearchText] = useState('');

    useEffect(() => {
        localStorage.setItem('key', JSON.stringify(products))
    },[products])


    const addProduct = () => {
        const imageUrl = img || 'https://w7.pngwing.com/pngs/250/929/png-transparent-no-symbol-no-smoking-miscellaneous-text-trademark.png';
            setProducts(prevState => [...prevState, {name, price, imageUrl}]);
        setName('');
        setPrice('');
        setImg('');
    };

    const deleteAllProducts = () => {
        setProducts([]);
    };

    const toggleFilter = () => {
        setFilterActive(prevState => !prevState);
    };

    const filterProducts = filterActive ? products.filter(product => product): products.filter(product => product.price >= 1000);


    const filteredProducts = filterProducts.filter((product) =>
        product.name.toLowerCase().includes(searchText.toLowerCase())
    );


    useEffect(() => {
        localStorage.setItem('key', JSON.stringify(products))
    },[products])

    return (
        <>
            <div className="nav">
                <h1>MARKET</h1>
                <input
                    className={'input'}
                    type="text"
                    placeholder="ищите тут..."
                    onChange={(e) => setSearchText(e.target.value)}
                />
                <button onClick={toggleFilter} className={'but2'}>от 1000</button>
                <button className="but1" onClick={deleteAllProducts} >
                    Удалить все
                </button>
            </div>
            <div className="glavnyi">
                <div className="container">
                    <h2>Продуктун аты</h2>
                    <input
                        type="text"
                        placeholder="аты"
                        onChange={(e) => setName(e.target.value)}
                        value={name}
                    />
                    <input
                        type="text"
                        placeholder="баасы"
                        onChange={(e) => setPrice(e.target.value)}
                        value={price}
                    />
                    <input
                        type="text"
                        placeholder="img url"
                        onChange={(e) => setImg(e.target.value)}
                        value={img}
                    />
                    <button
                        className="but"
                        disabled={!name || !price}
                        onClick={addProduct}
                    >
                        Добавить
                    </button>
                </div>
                {filteredProducts.map((product, index) => {
                    return (
                        <Deletet data={{product, index, setProducts}} key={index} />
                    )
                })}
            </div>
        </>
    );
}

export default App;
