import {useState} from "react";

function Deletet(props) {

    const [pere, setPere] = useState(true)

    const deleteProduct = (i) => {
        props.data.setProducts(prev => {
            const z = [...prev]
            z.splice(i, 1)
            return z
        })
    }

    const perevernut = () => {
        setPere(prevState => !prevState)
    }


    return (
        <div className={'container'}><img src={props.data.product.imageUrl} alt="" className={'img'}/>
            <h3>{props.data.product.name}</h3>            <span
                onClick={perevernut}>{pere ? props.data.product.price : (props.data.product.price / 84).toFixed(2)}
                {pere ? ` c` : ` $`}
            </span>
            <button className="button" onClick={() => deleteProduct(props.data.index)}>Удалить</button>
        </div>)
}

export default Deletet;